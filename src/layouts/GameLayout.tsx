import { Center, Stack } from "@chakra-ui/react";
import { PropsWithChildren } from "react";

const GameLayout: React.FunctionComponent<PropsWithChildren> = ({
  children,
}) => {
  return (
    <Stack height={"100vh"} direction={"column"}>
      <Center flexGrow={1}>{children}</Center>
    </Stack>
  );
};
export default GameLayout;
