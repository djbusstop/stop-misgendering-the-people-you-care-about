import { Box, Flex, Text } from "@chakra-ui/react";
import { PropsWithChildren } from "react";
import Link from "next/link";
import PageContainer from "@/components/PageContainer";

const DefaultLayout: React.FunctionComponent<PropsWithChildren> = ({
  children,
}) => {
  return (
    <>
      <Box background="whatsapp.500">
        <PageContainer>
          <Flex
            alignItems="center"
            width={"100%"}
            justifyContent={"space-between"}
          >
            <Link href={"/"}>
              <Text
                cursor={"pointer"}
                color="white"
                fontSize="2xl"
                fontWeight={"extrabold"}
              >
                GenderDuolingo
              </Text>
            </Link>
            <Link href={"/about"}>
              <Text
                as={"a"}
                color="white"
                fontSize="xl"
                fontWeight={"extrabold"}
                cursor="pointer"
                _hover={{
                  textDecoration: "underline",
                }}
              >
                About
              </Text>
            </Link>
          </Flex>
        </PageContainer>
      </Box>
      {children}
    </>
  );
};
export default DefaultLayout;
