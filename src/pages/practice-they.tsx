import generatePracticeTheyQuestions from "@/utils/generatePracticeTheyQuestions";
import Game from "@/components/Game";
import { GameProvider } from "@/contexts/GameContext";
import GameLayout from "@/layouts/GameLayout";

import { NextPageWithLayout } from "./_app";
import Head from "next/head";

const instructions = {
  title: "Practice using they/them pronouns.",
  description: `Assuming people's gender based on how they look leads to misgendering. 
  By calling people who's gender you don't know "they", you won't be as likely hurt
  their feelings.`,
};

const rules = {
  title: "How to play",
  description: `You don't know these people. You don't know their pronouns. So you should call them "they". Look at the picture of each person and when asked their pronouns,
  use "they".`,
};

const PracticeThey: NextPageWithLayout = () => {
  return (
    <>
      <Head>
        <title>GenderDuolingo - Practice They/Them Pronouns</title>
        <meta
          name="og:title"
          key="og:title"
          property="title"
          content={"GenderDuolingo - Practice They/Them Pronouns"}
        />
        <meta
          name="og:url"
          key="og:url"
          property="og:url"
          content={`https://genderduolingo.com/practice-they`}
        />
        <meta
          name="twitter:title"
          key="twitter:title"
          property="twitter:title"
          content={"GenderDuolingo - Practice They/Them Pronouns"}
        />
        <meta
          name="og:image"
          key="og:image"
          property="og:image"
          content={"https://genderduolingo.com/screenshots/practice-they.png"}
        />
        <meta
          name="twitter:image"
          key="twitter:image"
          property="twitter:image"
          content={"https://genderduolingo.com/screenshots/practice-they.png"}
        />
        <meta
          name="twitter:card"
          key="twitter:card"
          property="twitter:card"
          content="summary"
        />
      </Head>
      <GameProvider questions={generatePracticeTheyQuestions(10)}>
        <Game
          flagSrc="/nonbinary-flag.png"
          instructions={instructions}
          rules={rules}
        />
      </GameProvider>
    </>
  );
};

PracticeThey.getLayout = function getLayout(page) {
  return <GameLayout>{page}</GameLayout>;
};

export default PracticeThey;
