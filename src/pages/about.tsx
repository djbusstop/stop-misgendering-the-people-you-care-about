import { NextPageWithLayout } from "./_app";
import { useRouter } from "next/router";
import DefaultLayout from "@/layouts/DefaultLayout";
import PageContainer from "@/components/PageContainer";
import { Box, Link, Text } from "@chakra-ui/react";
import Head from "next/head";

const HomePage: NextPageWithLayout = () => {
  const router = useRouter();
  return (
    <>
      <Head>
        <title>GenderDuolingo - About</title>
      </Head>
      <PageContainer>
        <Box paddingBottom="8">
          <Text marginTop={"3"} fontSize={"4xl"} fontWeight="bold">
            About
          </Text>

          <Text marginTop={"4"}>
            The code for this project is{" "}
            <Link href="https://gitlab.com/djbusstop/stop-misgendering-the-people-you-care-about">
              <Text fontWeight={"bold"} color={"whatsapp.600"} as="a">
                open source
              </Text>
              .
            </Link>
          </Text>

          <Text marginTop={"4"}>
            Would you like your face to appear in the they/them quiz? If so,
            send rover a picture of you WHICH IS APPROPRIATE FOR THE TONE OF THE
            APPLICATION. If enough people want to be in it they will replace the
            current faces with your beautiful ones ❤️
          </Text>

          <Text marginTop={"4"}>
            The name of this app is satirical and in no way an attempt to
            mislead anyone in to believing this is a product from Duolingo, the
            language learning company I expect to get a cease and disist from if
            this gets too popular.
          </Text>

          <Text fontWeight={"bold"} fontSize="2xl" marginTop={"4"}>
            Contact
          </Text>

          <Text marginTop={"4"}>
            GenderDuolingo is created by the mysterious hacker known only as
            "rover". For more weird content check out{" "}
            <Link href="https://gaycommunism.noblogs.org">
              <Text fontWeight={"bold"} color={"whatsapp.600"} as="a">
                their blog
              </Text>
            </Link>
            . If you liked this, you might like{" "}
            <Link href="https://gaycommunism.noblogs.org/post/2022/02/23/the-agile-gender-manifesto/">
              <Text fontWeight={"bold"} color={"whatsapp.600"} as="a">
                The Agile Gender Manifesto
              </Text>
            </Link>
          </Text>

          <Text marginTop={"4"}>
            rover doesn't use social media so will have no way to know if you
            like this app, so if you do please send out positive energy directed
            at rover that they may feel it and have some sense that the time
            they spent on this was at all worthwhile.
          </Text>

          <Text marginTop={"4"}>
            To contact the mysterious hacker rover you can{" "}
            <Link href="mailto:mysteriousrover@protonmail.com">
              <Text fontWeight={"bold"} color={"whatsapp.600"} as="a">
                send them an email
              </Text>
            </Link>
            . They would be interested to hear about bugs, improvements, words
            of praise, marxist analyses of their work, offers to help, criticism
            (of contrapoints), other cool projects out there, and basically
            anything other than abuse or sexual harassment.
          </Text>
        </Box>
      </PageContainer>
    </>
  );
};

HomePage.getLayout = function getLayout(page) {
  return <DefaultLayout>{page}</DefaultLayout>;
};

export default HomePage;
