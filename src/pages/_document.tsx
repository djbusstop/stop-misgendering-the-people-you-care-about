import { Html, Head, Main, NextScript } from "next/document";

const title = "GenderDuolingo";
const description = `Like Duolingo, but for learning to use people's correct pronouns. You say you want to get better, so here's your chance. No, I didn't make this website out of spite.`;

export default function Document() {
  return (
    <Html>
      <Head>
        <meta name="description" content={description} />

        {/* Open graph - https://ogp.me/  */}
        <meta name="og:title" key="og:title" property="title" content={title} />
        <meta
          name="og:description"
          key="og:description"
          property="og:description"
          content={description}
        />
        <meta
          name="og:image"
          key="og:image"
          property="og:image"
          content={"https://genderduolingo.com/screenshots/twitter-card.jpg"}
        />
        <meta
          name="og:url"
          key="og:url"
          property="og:url"
          content={`https://genderduolingo.com`}
        />
        <meta
          property="og:site_name"
          name="og:site_name"
          content="GenderDuolingo"
        />
        <meta property="og:type" name="og:type" content="website" />
        {/* Twitter */}
        <meta
          name="twitter:title"
          key="twitter:title"
          property="twitter:title"
          content={title}
        />
        <meta
          name="twitter:description"
          key="twitter:description"
          property="twitter:description"
          content={description}
        />
        <meta
          name="twitter:image"
          key="twitter:image"
          property="twitter:image"
          content={"https://genderduolingo.com/screenshots/twitter-card.jpg"}
        />
        <meta
          name="twitter:image:alt"
          key="twitter:image:alt"
          property="twitter:image:alt"
          content={title}
        />
        <meta
          name="twitter:card"
          key="twitter:card"
          property="twitter:card"
          content="summary"
        />

        {/* Icons */}
        <link rel="icon" href="/nonbinary-flag.png" sizes="any" />
        <link rel="apple-touch-icon" href="/nonbinary-flag.png" />
      </Head>
      <body>
        <Main />
        <NextScript />
      </body>
    </Html>
  );
}
