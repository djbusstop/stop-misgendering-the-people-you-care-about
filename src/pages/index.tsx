import DefaultLayout from "@/layouts/DefaultLayout";

import { NextPageWithLayout } from "./_app";
import {
  Box,
  Button,
  ListItem,
  Text,
  Image,
  UnorderedList,
  Grid,
} from "@chakra-ui/react";
import { useRouter } from "next/router";
import PageContainer from "@/components/PageContainer";
import Head from "next/head";
import Link from "next/link";

const HomePage: NextPageWithLayout = () => {
  const router = useRouter();
  return (
    <>
      <Head>
        <title>GenderDuolingo</title>
        <link
          key={"nonbinary-flag"}
          rel="preload"
          as="image"
          href={"/nonbinary-flag.png"}
        />
      </Head>

      {/* Hero */}
      <Box
        paddingX={["3", "5"]}
        paddingY={["5", "5", "7"]}
        background="blue.600"
      >
        <PageContainer>
          <Text
            marginBottom="4"
            fontWeight="bold"
            color="white"
            fontSize={["3xl", "4xl"]}
          >
            Like Duolingo, but for learning to use people's correct pronouns.
          </Text>
          <Button
            colorScheme={"whatsapp"}
            onClick={() => router.push("/practice-they")}
            marginBottom="2"
            display={"flex"}
            alignItems="center"
          >
            <Image
              alt="nonbinary flag"
              src="/nonbinary-flag.png"
              height={"60%"}
              width="30px"
              borderRadius="base"
              marginRight={"2"}
              loading="eager"
            />
            Practice They/Them Now!
          </Button>
        </PageContainer>
      </Box>

      {/* 3 item list */}
      <Box
        paddingX={["3", "5"]}
        paddingY={["5", "5", "7"]}
        background="gray.50"
        borderBottom={"1px #e2e2e2 solid"}
      >
        <PageContainer maxWidth={["sm", "sm", "3xl"]}>
          <Grid gap="5" gridTemplateColumns={["1fr", "1fr", "repeat(3, 1fr)"]}>
            <Box>
              <Text fontSize="2xl" fontWeight="bold" textAlign={"center"}>
                Safe
              </Text>
              <Text textAlign={"center"}>
                You can learn without making people feel like garbage like you
                probably have been doing. If you misgender someone in this app
                it doesn't hurt their feelings, it's just a picture.
              </Text>
            </Box>
            <Box>
              <Text fontSize="2xl" fontWeight="bold" textAlign={"center"}>
                Convenient
              </Text>
              <Text textAlign={"center"}>
                Practice using the right pronouns on public transit, while on
                the toilet, or instead of scrolling Instagram. You say you want
                to get better - now is the time.
              </Text>
            </Box>
            <Box>
              <Text fontSize="2xl" fontWeight="bold" textAlign={"center"}>
                Connected
              </Text>
              <Text textAlign={"center"}>
                Your family member, colleague, or friend might actually want to
                be around you if you use the right pronouns for them.
              </Text>
            </Box>
          </Grid>
        </PageContainer>
      </Box>

      {/* Lessons */}
      <Box
        paddingX={["3", "5"]}
        paddingY={["5", "5", "7"]}
        borderBottom={"1px #e2e2e2 solid"}
      >
        <PageContainer>
          <Box>
            <Text
              marginTop={"-2"}
              fontWeight={"bold"}
              fontSize={["3xl", "4xl"]}
            >
              Lessons
            </Text>
            <UnorderedList listStyleType={"none"} marginTop="2" spacing={"2"}>
              <ListItem>
                <Link href="/practice-they" passHref>
                  <Text
                    color={"whatsapp.500"}
                    fontSize={["1xl", "2xl"]}
                    fontWeight="bold"
                    cursor="pointer"
                    _hover={{ textDecoration: "underline" }}
                    as="a"
                    display={"flex"}
                    alignItems="center"
                  >
                    <Image
                      display={"inline-block"}
                      alt="nonbinary flag"
                      src="/nonbinary-flag.png"
                      height={"23px"}
                      width="30px"
                      borderRadius="base"
                      marginRight={"2"}
                      loading="eager"
                    />
                    Practice They/Them
                  </Text>
                </Link>
              </ListItem>
              <ListItem>
                <Text
                  fontSize={["1xl", "2xl"]}
                  color="gray.400"
                  fontWeight="bold"
                  cursor="not-allowed"
                >
                  Custom pronouns & photos (Coming soon)
                </Text>
              </ListItem>
            </UnorderedList>
          </Box>
        </PageContainer>
      </Box>

      {/* Coming Soon */}
      <Box
        paddingX={["3", "5"]}
        paddingY={["5", "5", "7"]}
        paddingBottom={["5", "7", "9"]}
        background={"gray.100"}
      >
        <PageContainer>
          <Box>
            <Text fontWeight={"bold"} fontSize={["2xl"]}>
              Under Construction
            </Text>
            <Text marginTop="2">
              Custom pronouns & photos: A future feature where you can upload
              your pronouns, name, and photos to generate a lesson about you,
              which you can send to whoever needs to stop fucking misgendering
              you.
            </Text>
          </Box>
        </PageContainer>
      </Box>
    </>
  );
};

HomePage.getLayout = function getLayout(page) {
  return <DefaultLayout>{page}</DefaultLayout>;
};

export default HomePage;
