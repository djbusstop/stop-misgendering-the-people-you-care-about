const range = (end: number) => {
  return [...Array(end).keys()];
};

export default range;
