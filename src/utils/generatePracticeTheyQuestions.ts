import {
  ENGLISH_PRONOUNS,
  MultipleChoiceOption,
  QUESTION_TYPE,
  Questions,
} from "@/types";
import randomInt from "./randomInt";
import range from "./range";
import shuffle from "./shuffle";

const DEFAULT_QUESTION = "What pronoun should you use for this person?";

// List of sentences where verb is conjugated for "they"
const theySentences = [
  "{} are eating dinner",
  "Are {} coming over?",
  "{} are having a party",
  "{} want me to call them the correct pronouns",
  "{} are my favourite colleague",
  "Who are {} dating?",
  "Do {} have your number?",
  "Where do {} live?",
  "Do {} like dancing?",
  "I think {} are great",
];

const NUM_THEY_PICS = 32;

const generatePracticeTheyQuestions = (numberQuestions = 5): Questions => {
  const questions: Questions = range(numberQuestions).map((i) => {
    if ((i + 1) / numberQuestions < 0.75) {
      // Until 75%, generate multiple choice questions
      return {
        type: QUESTION_TYPE.MULPITLE_CHOICE,
        id: i,
        imgUrl: `/they-thems/they-${randomInt(NUM_THEY_PICS, 1)}.jpeg`,
        question: DEFAULT_QUESTION,
        options: shuffle(
          // Generates a random order list of pronouns where only they is correct
          Object.values(ENGLISH_PRONOUNS).map(
            (pronoun): MultipleChoiceOption => ({
              label: pronoun,
              correct: pronoun === ENGLISH_PRONOUNS.THEY,
            })
          )
        ),
      };
    } else {
      // Todo: rewrite functionally
      // Questions show up only once unless run out of questions
      // let sentence: string;
      // const sentenceIndex = randomInt(theySentencesCopy.length);
      // const secondCopy = [...theySentencesCopy];
      // sentence = secondCopy[sentenceIndex];
      // if (theySentencesCopy.length === 0) {
      //   sentence = theySentences[randomInt(theySentences.length)];
      // }

      return {
        type: QUESTION_TYPE.WRITE_IN,
        imgUrl: `/they-thems/they-${randomInt(NUM_THEY_PICS, 1)}.jpeg`,
        id: i,
        question: DEFAULT_QUESTION,
        pronoun: ENGLISH_PRONOUNS.THEY,
      };
    }
  });

  return shuffle(questions);
};

export default generatePracticeTheyQuestions;
