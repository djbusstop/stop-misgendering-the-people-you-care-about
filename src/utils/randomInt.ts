/**
 * Generates a random integer from 0 to the max number, inclusive.
 */
const randomInt = (max: number, start: number = 0) => {
  return Math.floor(Math.random() * max + start);
};

export default randomInt;
