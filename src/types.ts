export enum ENGLISH_PRONOUNS {
  SHE = "she",
  THEY = "they",
  HE = "he",
}

export enum QUESTION_TYPE {
  MULPITLE_CHOICE = "multiple_choice",
  WRITE_IN = "write_in",
}

export interface Game {
  instructions: {
    title: string;
    paragraph: string;
  };
  description: {
    title: string;
    paragraph: string;
  };
  questions: Questions;
}

export type FnAnswerQuestion = (question: Question, answer: Answer) => void;

export type Question = MultipleChoiceQuestion | WriteInQuestion;
export type Questions = Array<Question>;
export type Answer = MultipleChoiceOption | WriteInQuestionAnswer;
export type AnsweredQuestion =
  | AnsweredMultipleChoiceQuestion
  | AnsweredWriteInQuestion;
export type AnsweredQuestions = Array<AnsweredQuestion>;

// Multiple Choice Questions

export interface MultipleChoiceQuestion {
  type: QUESTION_TYPE.MULPITLE_CHOICE;
  imgUrl: string;
  id: number;
  question: string;
  options: MultipleChoiceOption[];
}

export interface AnsweredMultipleChoiceQuestion extends MultipleChoiceQuestion {
  answer: MultipleChoiceOption;
}

export interface MultipleChoiceOption {
  label: string;
  correct: boolean;
}

// Write In Questions

export interface WriteInQuestion {
  type: QUESTION_TYPE.WRITE_IN;
  imgUrl: string;
  id: number;
  question: string;
  pronoun: ENGLISH_PRONOUNS;
}

export interface WriteInQuestionAnswer {
  userInput: string;
  correct: boolean;
  correctAnswer: string;
}

export interface AnsweredWriteInQuestion extends WriteInQuestion {
  answer: WriteInQuestionAnswer;
}
