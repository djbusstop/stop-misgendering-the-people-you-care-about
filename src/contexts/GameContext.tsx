import {
  Questions,
  AnsweredQuestions,
  FnAnswerQuestion,
  Question,
  AnsweredQuestion,
  Answer,
  QUESTION_TYPE,
} from "@/types";
import React, { createContext, PropsWithChildren, useState } from "react";

interface GameContextProps {
  questions: Questions;
  answeredQuestions: AnsweredQuestions;
  currentQuestion: Question;
  goToNextQuestion: () => void;
  numberCorrect: number;
  answerQuestion: FnAnswerQuestion;
  reset: () => void;
}

const GameContext = createContext<GameContextProps>({
  questions: [],
  answeredQuestions: [],
  currentQuestion: {
    type: QUESTION_TYPE.MULPITLE_CHOICE,
    imgUrl: "",
    id: 1,
    question: "",
    options: [],
  },
  goToNextQuestion: () => undefined,
  numberCorrect: 0,
  answerQuestion: () => undefined,
  reset: () => undefined,
});

export const GameProvider: React.FunctionComponent<
  PropsWithChildren<{
    questions: Questions;
  }>
> = ({ children, questions }) => {
  // Game State
  const [answeredQuestions, setAnsweredQuestions] = useState<AnsweredQuestions>(
    []
  );
  const [currentQuestionIndex, setCurrentQuestionIndex] = useState<number>(0);
  const currentQuestion = questions[currentQuestionIndex];

  // Counts number of answered questions with correct answer
  const numberCorrect = answeredQuestions.reduce((acc: number, question) => {
    if (question.answer.correct) {
      acc = acc + 1;
    }
    return acc;
  }, 0);

  const reset = () => {
    setAnsweredQuestions([]);
    setCurrentQuestionIndex(0);
  };

  const answerQuestion = (question: Question, answer: Answer) => {
    const answeredQuestion = {
      ...question,
      answer,
    } as AnsweredQuestion;
    const answers = [...answeredQuestions, answeredQuestion];
    setAnsweredQuestions(answers);
  };

  const goToNextQuestion = () => {
    setCurrentQuestionIndex(currentQuestionIndex + 1);
  };

  return (
    <GameContext.Provider
      value={{
        // Config
        questions,
        // Game State
        currentQuestion,
        goToNextQuestion,
        answeredQuestions,
        numberCorrect,
        // Helpers
        answerQuestion,
        reset,
      }}
    >
      {children}
    </GameContext.Provider>
  );
};

export default GameContext;
