import { Container } from "@chakra-ui/react";
import { PropsWithChildren } from "react";

const PageContainer = ({
  maxWidth,
  children,
}: PropsWithChildren<{ maxWidth?: string | string[] }>) => {
  return (
    <Container padding="3" width={"100%"} maxWidth={maxWidth || "3xl"}>
      {children}
    </Container>
  );
};

export default PageContainer;
