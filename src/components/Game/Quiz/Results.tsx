import GameContext from "@/contexts/GameContext";
import randomInt from "@/utils/randomInt";
import { Button, Stack, Text } from "@chakra-ui/react";
import { useRouter } from "next/router";
import { useContext } from "react";

// Arrays with different messages for 20% intervals
const feedbackMessages = [
  {
    until: 49,
    emojis: "🤬👎💣🤕",
    messages: [
      "That's truly abysmal... You should be ashamed",
      "Do better. Honestly, you're really bad.",
      "You're hurting the people you claim to love",
    ],
  },
  {
    until: 75,
    emojis: "😔💩🤦🪱",
    messages: [
      "I see you trying but it's not enough",
      "This isn't good enough. Try harder.",
      "What if someone only got YOUR pronouns right that often?",
    ],
  },
  {
    until: 99,
    emojis: "🥹☠️🙏",
    messages: [
      "Almost perfect, but not quite enough.",
      "Keep practicing, you'll get there.",
      "Don't be too proud of yourself, you still need to improve",
    ],
  },
  {
    until: 100,
    emojis: "🎉😇🥰",
    messages: [
      "You did it! Go tell someone how good you are at using they pronouns!",
      "You have finally mastered pronouns. Now send this app to 10 people.",
      "What a result! I couldn't be more proud of you.",
      "Imagine if everyone was as good as you! The world would be so much better.",
    ],
  },
];

const Results = () => {
  const { numberCorrect, questions } = useContext(GameContext);
  const router = useRouter();
  const percentage = (numberCorrect / questions.length) * 100;

  const messagesForRange = feedbackMessages.find(
    (range) => percentage <= range.until
  );

  if (!messagesForRange) throw new Error("You need to write more messages");

  const feedback =
    messagesForRange.messages[randomInt(messagesForRange.messages.length)];

  return (
    <Stack spacing="10" alignItems="center">
      <Stack
        spacing={0}
        direction={"row"}
        alignItems="center"
        justifyContent={"center"}
      >
        <Text fontSize="8xl">{numberCorrect}</Text>
        <Text fontSize="5xl">&nbsp;/&nbsp;{questions.length}</Text>
      </Stack>
      <Text fontSize={"5xl"}>{messagesForRange.emojis}</Text>
      <Text textAlign="center" fontSize={"2xl"}>
        {feedback}
      </Text>
      <Button width={"100%"} onClick={() => router.reload()}>
        Restart
      </Button>
    </Stack>
  );
};

export default Results;
