import GameContext from "@/contexts/GameContext";
import { WriteInQuestion } from "@/types";
import { Button, Center, Image, Input, Stack, Text } from "@chakra-ui/react";
import { useContext, useEffect, useRef, useState } from "react";
import { compareTwoStrings } from "string-similarity";

const WriteInQuestionInput = ({
  disabled,
  question,
}: {
  disabled: boolean;
  question: WriteInQuestion;
}) => {
  const { answerQuestion } = useContext(GameContext);
  const [userInput, setUserInput] = useState("");
  const inputRef = useRef<HTMLInputElement>(null);

  useEffect(() => {
    // Focus input on page load
    if (inputRef.current) {
      inputRef.current.focus();
    }
    // If question changes, reset input
    if (question) {
      setUserInput("");
    }
  }, [inputRef, question]);

  const isAnswerCorrect =
    compareTwoStrings(userInput.toLowerCase(), question.pronoun) >= 0.8 &&
    userInput.toLowerCase().includes(question.pronoun.toLowerCase());

  return (
    <>
      <Stack height="100%" justifyContent={"space-between"}>
        <Center flexGrow={1}>
          <Stack spacing={4} direction={"column"}>
            <Image
              borderRadius="md"
              loading="eager"
              src={question.imgUrl}
              alt="This person's pronouns are they/them"
            />
            <Text
              color={"gray.700"}
              fontSize="2xl"
              textAlign={"center"}
              fontWeight="bold"
            >
              {question.question}
            </Text>
          </Stack>
        </Center>

        <form
          onSubmit={(e) => {
            e.preventDefault();
            answerQuestion(question, {
              userInput,
              correct: isAnswerCorrect,
              correctAnswer: question.pronoun,
            });
          }}
        >
          <Stack spacing={5}>
            <Input
              ref={inputRef}
              isDisabled={disabled}
              value={userInput}
              placeholder="Write your answer here..."
              onChange={(e) => setUserInput(e.target.value)}
              type="text"
            />
            <Button
              colorScheme={"blue"}
              width={"100%"}
              disabled={disabled || userInput.length === 0}
              type="submit"
            >
              Submit
            </Button>
          </Stack>
        </form>
      </Stack>
    </>
  );
};

export default WriteInQuestionInput;
