import { Button, useToast } from "@chakra-ui/react";
import { useContext, useEffect, useRef, useState } from "react";

import { Question, QUESTION_TYPE } from "@/types";
import GameContext from "@/contexts/GameContext";
import MultipleChoiceQuestionInput from "./MultipleChoiceQuestionInput";
import WriteInQuestionInput from "./WriteInQuestionInput";
import generateErrorToastText from "./generateErrorToastText";

const QuestionInput = () => {
  const { questions, answeredQuestions, currentQuestion, goToNextQuestion } =
    useContext(GameContext);
  const toast = useToast();

  useEffect(() => {
    const lastAnsweredQuestion =
      answeredQuestions[answeredQuestions.length - 1];
    // If last answered question is different than current question
    if (
      lastAnsweredQuestion &&
      lastAnsweredQuestion.id === currentQuestion.id
    ) {
      toast({
        title: lastAnsweredQuestion.answer.correct ? "Hooray!" : "WRONG!",
        description: generateErrorToastText(
          currentQuestion,
          lastAnsweredQuestion.answer
        ),
        status: lastAnsweredQuestion.answer.correct ? "success" : "error",
        duration: 5000,
        position: "top",
        isClosable: true,
        colorScheme: "whatsapp",
      });
    }
  }, [toast, currentQuestion, answeredQuestions]);

  const lastAnsweredQuestion = answeredQuestions[answeredQuestions.length - 1];
  const currentQuestionHasBeenAnswered =
    lastAnsweredQuestion && lastAnsweredQuestion.id === currentQuestion.id;

  return (
    <>
      {/* Question renderer */}
      {currentQuestion.type === QUESTION_TYPE.MULPITLE_CHOICE && (
        <MultipleChoiceQuestionInput
          disabled={currentQuestionHasBeenAnswered}
          question={currentQuestion}
        />
      )}

      {currentQuestion.type === QUESTION_TYPE.WRITE_IN && (
        <WriteInQuestionInput
          disabled={currentQuestionHasBeenAnswered}
          question={currentQuestion}
        />
      )}
      {/* If current question has been answered */}
      {currentQuestionHasBeenAnswered && (
        <Button
          colorScheme={"whatsapp"}
          size={"lg"}
          onClick={() => {
            toast.closeAll();
            goToNextQuestion();
          }}
        >
          {answeredQuestions.length === questions.length
            ? "Finish"
            : "Next question"}
        </Button>
      )}
    </>
  );
};

export default QuestionInput;
