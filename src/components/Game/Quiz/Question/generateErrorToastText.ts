import {
  Answer,
  Question,
  QUESTION_TYPE,
  WriteInQuestionAnswer,
} from "@/types";

const generateErrorToastText = (question: Question, answer: Answer): string => {
  return answer.correct
    ? "Yes well done!!"
    : `NO!! NO!!! The correct answer was "${
        question.type === QUESTION_TYPE.MULPITLE_CHOICE
          ? question.options.find((option) => option.correct)?.label
          : question.type === QUESTION_TYPE.WRITE_IN
          ? (answer as WriteInQuestionAnswer).correctAnswer
              .charAt(0)
              .toUpperCase() +
            (answer as WriteInQuestionAnswer).correctAnswer.slice(1)
          : ""
      }"`;
};

export default generateErrorToastText;
