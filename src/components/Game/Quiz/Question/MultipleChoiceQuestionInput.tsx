import GameContext from "@/contexts/GameContext";
import { MultipleChoiceQuestion, FnAnswerQuestion, Answer } from "@/types";
import { Box, Button, Center, Image, Stack, Text } from "@chakra-ui/react";
import { useContext } from "react";

const MultipleChoiceQuestionInput = ({
  disabled,
  question,
}: {
  disabled: boolean;
  question: MultipleChoiceQuestion;
}) => {
  const { answerQuestion } = useContext(GameContext);
  return (
    <>
      <Stack height="100%" justifyContent={"space-between"}>
        <Center flexGrow={1}>
          <Stack spacing={3} direction={"column"}>
            <Image
              borderRadius="md"
              loading="eager"
              src={question.imgUrl}
              alt="This person's pronouns are they/them"
            />
            <Text
              color={"gray.700"}
              textAlign={"center"}
              fontSize={"2xl"}
              fontWeight={"bold"}
            >
              {question.question}
            </Text>
          </Stack>
        </Center>
        <Stack direction={"row"} justifyContent="space-between">
          {question.options.map((option, i) => {
            return (
              <Button
                variant={"outline"}
                colorScheme={"blue"}
                size={"lg"}
                disabled={disabled}
                key={i}
                onClick={() => {
                  answerQuestion(question, option);
                }}
              >
                {option.label}
              </Button>
            );
          })}
        </Stack>
      </Stack>
    </>
  );
};

export default MultipleChoiceQuestionInput;
