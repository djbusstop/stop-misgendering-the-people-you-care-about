import { useContext } from "react";
import Image from "next/image";

import GameContext from "@/contexts/GameContext";
import QuestionInput from "./Question";
import Results from "./Results";
import { Progress } from "@chakra-ui/react";

const Quiz = () => {
  const { questions, answeredQuestions, currentQuestion } =
    useContext(GameContext);

  const stillAnotherQuestion = answeredQuestions.length < questions.length;

  // If there are still questions to answer
  if (stillAnotherQuestion) {
    return (
      <>
        <Progress
          colorScheme={"whatsapp"}
          value={(answeredQuestions.length / questions.length) * 100}
        />
        <QuestionInput />
      </>
    );
  }

  // If done, show results
  return <Results />;
};

export default Quiz;
