import { useContext, useState } from "react";

import GameContext from "@/contexts/GameContext";
import Quiz from "./Quiz";
import {
  Box,
  Button,
  Flex,
  Heading,
  Image,
  Stack,
  Text,
} from "@chakra-ui/react";
import { useRouter } from "next/router";
import ConfirmQuitDialog from "./ConfirmQuitDialog";
import Head from "next/head";

interface PreGameSection {
  title: string;
  description: string;
}

const Game = ({
  flagSrc,
  instructions,
  rules,
}: {
  flagSrc?: string;
  instructions: PreGameSection;
  rules: PreGameSection;
}) => {
  const [showInstructions, setShowInstructions] = useState(false);
  const [startGame, setStartGame] = useState(false);
  const router = useRouter();

  const [showQuitDialog, setShowQuitDialog] = useState(false);

  const { questions, reset, answeredQuestions } = useContext(GameContext);

  return (
    <>
      <Head>
        {questions.map((question, i) => (
          <link
            key={question.imgUrl}
            rel="preload"
            as="image"
            href={question.imgUrl}
          />
        ))}
      </Head>
      <Box height={"100%"} width={"350px"} borderRadius={5} padding={5}>
        <Stack
          height="100%"
          justifyContent={"space-between"}
          direction={"column"}
          spacing={3}
        >
          {/* Button top row */}
          <Flex flexDirection={"row-reverse"} gap={3} alignItems={"center"}>
            <Button
              onClick={() => {
                if (startGame || answeredQuestions.length > 0) {
                  // Confirm quit when game started
                  setShowQuitDialog(true);
                } else {
                  router.push("/");
                }
              }}
            >
              Quit
            </Button>
            <ConfirmQuitDialog
              isOpen={showQuitDialog}
              onConfirm={() => {
                // Resets the game state
                reset();
                // Navigate home
                router.push("/");
              }}
              onClose={() => setShowQuitDialog(false)}
            />
            {startGame && ( // Show button to see rules if game started
              <Button
                onClick={() => {
                  setStartGame(false);
                  setShowInstructions(true);
                }}
                size={"sm"}
              >
                Rules
              </Button>
            )}
          </Flex>
          {/* Text */}
          {!startGame && !showInstructions && (
            <>
              <Stack direction={"column"}>
                {flagSrc && (
                  <Image
                    borderRadius={"base"}
                    src={flagSrc}
                    aria-label="nonbinary flag"
                    marginBottom={"5"}
                  />
                )}
                <Heading as="h3" size="lg">
                  {instructions.title}
                </Heading>
                <Text>{instructions.description}</Text>
              </Stack>
              <Button
                colorScheme="whatsapp"
                width={"100%"}
                onClick={() => setShowInstructions(true)}
              >
                Next
              </Button>
            </>
          )}
          {showInstructions && !startGame && (
            <>
              <Stack direction={"column"}>
                <Heading as="h3" size="lg">
                  {rules.title}
                </Heading>
                <Text>{rules.description}</Text>
              </Stack>
              <Button
                colorScheme={"whatsapp"}
                width={"100%"}
                onClick={() => setStartGame(true)}
              >
                {answeredQuestions.length > 0 ? "Return" : "Start"}
              </Button>
            </>
          )}
          {/* Quiz */}
          {startGame && <Quiz />}
        </Stack>
      </Box>
    </>
  );
};

export default Game;
